//This is kinda funky

#include "CTestFramework.h"
#include <iostream>
#include <string>

using namespace std;

int main(int argc,char *argv[])
{
    bool vsync = false;
    bool automode = false;

    for(unsigned int i = 0; i < argc; i++)
    {
        std::string s = argv[i];

        if(s == "-h" || s == "--help")
        {
            std::cout << "RenderTest, an irrRenderer demo." << std::endl;
            std::cout << "Compiled with Irrlicht " << IRRLICHT_SDK_VERSION;
#ifdef __GNUC__
            std::cout << ", GCC " << __VERSION__;
#endif // __GNUC__
            std::cout << std::endl << std::endl;
            std::cout << "irrRenderer " << IRRRENDERER_VERSION << std::endl;
#ifdef COMPILE_WITH_IRRPP
            std::cout << "irrPP " << IRRPP_VERSION << std::endl;
#endif // COMPILE_WITH_IRRPP
            std::cout << std::endl;
            std::cout << "Arguments:" << std::endl;
            std::cout << "\t-v --vsync\n" << "\t\t Start with vertical synchronization enabled\n";
            std::cout << "\t-a --auto\n" << "\t\t Start in full screen mode, autodetect desktop resolution\n";
            std::cout << "\t-h --help\n" << "\t\t Print this message\n";

            return 0;
        }
        else if(s == "--vsync" || s == "-v")
        {
            vsync = true;
        }
        else if(s == "--auto" || s == "-a")
        {
            automode = true;
        }
    }

    CTestFramework* test= new CTestFramework(vsync, automode);
    while(test->run())
    {
        //funky,
        //funky,
        //funky!
    }

    delete test;
    return 0;
}
