#ifndef CTESTFRAMEWORK_H
#define CTESTFRAMEWORK_H

#include <irrlicht.h>
#include <irrRenderer.h>
#include <iostream>

//! comment out the following line in order to compile without irrPP
#define COMPILE_WITH_IRRPP

#ifdef COMPILE_WITH_IRRPP
#include <irrPP.h>
#endif

using namespace irr;

class CTestFramework : public IEventReceiver
{
public:
    CTestFramework(bool vsync, bool automode);

    ~CTestFramework();

    bool run();

    virtual bool OnEvent(const SEvent& event);

private:
    IrrlichtDevice* Device;
    video::CRenderer* Renderer;
#ifdef COMPILE_WITH_IRRPP
    video::irrPP* Pp;
    video::CPostProcessingEffect* AA;
    video::CPostProcessingEffectChain* Bloom;
#endif

    bool RequestedExit;
    irr::u32 LastFPS;

    bool Vsync, Fullscreen;
    u32 Depth;
    core::dimension2d<u32> Resolution;

    scene::ILightSceneNode* Flashlight;

    gui::IGUIStaticText* Console, *Help;
    bool DrawGBuffer;
    irr::u32 timeLast, timeSinceLastEvent;
};

#endif // CTESTFRAMEWORK_H
