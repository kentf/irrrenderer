uniform mat4 WorldViewProjMat;
uniform mat4 WorldViewMat;
uniform float CamFar;
uniform float Lighting;

varying vec3 Normal;
varying float Depth;

void main()
{
    Normal = normalize((gl_NormalMatrix * gl_Normal)).xyz;

    vec4 vertex = WorldViewProjMat * gl_Vertex;
    Depth = vertex.z/CamFar;

    gl_Position = vertex;
    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_TexCoord[1] = gl_MultiTexCoord0;
}
