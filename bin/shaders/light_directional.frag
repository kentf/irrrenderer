uniform sampler2D ColorTex;
uniform sampler2D NormalTex;    //view space normal.xyz
uniform sampler2D DepthTex;    //view space depth
uniform vec3 Direction;
uniform vec3 Color;

void main()
{
    vec3 vNormal= texture2D(NormalTex, gl_TexCoord[0].xy).xyz;

    //reconstruct normal
    vNormal.xyz*= 2.0;
    vNormal.xyz-= 1.0;

    //calculate the light
    vec4 lightDir = vec4(normalize(Direction), 0.0);
    float light = max(dot(lightDir, vNormal), 0.0);

    gl_FragColor = light * vec4(Color, 0.0) * texture2D(ColorTex, gl_TexCoord[0].xy);
}
